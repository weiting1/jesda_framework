<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 1/5/17
 * Time: 21:39
 */

namespace JesdaLib\BusinessRule;
use PHPUnit\Framework\TestCase;

class ApplyStuBRTest extends TestCase
{
    public function testSaveFormData()
    {
        $applyStuBR = new ApplyStuBR();

        $result = $applyStuBR->saveFormData(1, '{}', 'surveyForm');
        $this->assertEquals($result, "kevin102");
        $this->assertEmpty($result);

    }
}
