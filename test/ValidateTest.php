<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 1/5/17
 * Time: 21:39
 */

namespace Jesda\Validate;
use PHPUnit\Framework\TestCase;

class ValidateTest extends TestCase
{
    public function testIntIsValid()
    {
        $validateInt = new ValidateInt();

        $result = $validateInt->isValid(10);
        $this->assertEquals($result, true);
        $result = $validateInt->isValid(10.0);
        $this->assertEquals($result, true);
        $result = $validateInt->isValid('10');
        $this->assertEquals($result, true);
        $result = $validateInt->isValid('10.0');
        $this->assertEquals($result, true);
        $result = $validateInt->isValid(0x10);
        $this->assertEquals($result, true);
        $result = $validateInt->isValid('10.1');
        $this->assertEquals($result, false);
        $result = $validateInt->isValid(10.1);
        $this->assertEquals($result, false);
        $result = $validateInt->isValid('10y');
        $this->assertEquals($result, false);
        $result = $validateInt->isValid([10]);
        $this->assertEquals($result, false);
        $result = $validateInt->isValid(null);
        $this->assertEquals($result, false);
        $result = $validateInt->isValid('');
        $this->assertEquals($result, false);


    }

    public function testFloatIsValid()
    {
        $validateFloat = new ValidateFloat();

        $result = $validateFloat->isValid(10);
        $this->assertEquals($result, true);
        $result = $validateFloat->isValid(10.0);
        $this->assertEquals($result, true);
        $result = $validateFloat->isValid('10');
        $this->assertEquals($result, true);
        $result = $validateFloat->isValid('10.0');
        $this->assertEquals($result, true);
        $result = $validateFloat->isValid(0x10);
        $this->assertEquals($result, true);
        $result = $validateFloat->isValid('10.1');
        $this->assertEquals($result, true);
        $result = $validateFloat->isValid(10.1);
        $this->assertEquals($result, true);
        $result = $validateFloat->isValid('10y');
        $this->assertEquals($result, false);
        $result = $validateFloat->isValid([10]);
        $this->assertEquals($result, false);
        $result = $validateFloat->isValid(null);
        $this->assertEquals($result, false);
        $result = $validateFloat->isValid('');
        $this->assertEquals($result, false);
    }

    public function testDateIsValid()
    {
        $validateDate = new ValidateDate();

        $result = $validateDate->isValid('');
        $this->assertEquals($result, false);

        $result = $validateDate->isValid('test');
        $this->assertEquals($result, false);

        $result = $validateDate->isValid('2016-10-10');
        $this->assertEquals($result, true);

        $result = $validateDate->isValid('2016-05-05');
        $this->assertEquals($result, true);

        $result = $validateDate->isValid('2016-05-32');
        $this->assertEquals($result, false);

        $result = $validateDate->isValid('2016/05/10');
        $this->assertEquals($result, false);

        $result = $validateDate->isValid(20160510);
        $this->assertEquals($result, false);
    }

    public function testNotEmptyIsValid()
    {
        $notEmpty = new NotEmpty();

        $result = $notEmpty->isValid('');
        $this->assertEquals($result, false);

        $result = $notEmpty->isValid('   ');
        $this->assertEquals($result, false);

        $result = $notEmpty->isValid(null);
        $this->assertEquals($result, false);

        $result = $notEmpty->isValid([]);
        $this->assertEquals($result, false);

        $result = $notEmpty->isValid(0);
        $this->assertEquals($result, true);

        $result = $notEmpty->isValid('0');
        $this->assertEquals($result, true);

        $result = $notEmpty->isValid('test');
        $this->assertEquals($result, true);

        $result = $notEmpty->isValid(1);
        $this->assertEquals($result, true);

        $result = $notEmpty->isValid([1]);
        $this->assertEquals($result, true);

        $result = $notEmpty->isValid(new NotEmpty());
        $this->assertEquals($result, true);

        $result = $notEmpty->setType(NotEmpty::ZERO)->isValid('0');
        $this->assertEquals($result, false);
    }

    public function testLessGreaterThanValid()
    {
        $validate = new LessThan();
        $this->assertEquals($validate->isValid(15, 20), true);
        $this->assertEquals($validate->isValid(20, 20), false);
        $this->assertEquals($validate->isValid(25, 20), false);

        $validate = new LessThan();
        $this->assertEquals($validate->isValid('a', 'b'), true);
        $this->assertEquals($validate->isValid('b', 'b'), false);
        $this->assertEquals($validate->isValid('z', 'b'), false);

        $validate = new GreaterThan();
        $this->assertEquals($validate->isValid(15, 20), false);
        $this->assertEquals($validate->isValid(20, 20), false);
        $this->assertEquals($validate->isValid(25, 20), true);

        $validate = new GreaterThan();
        $this->assertEquals($validate->isValid('a', 'b'), false);
        $this->assertEquals($validate->isValid('b', 'b'), false);
        $this->assertEquals($validate->isValid('z', 'b'), true);
    }
}
