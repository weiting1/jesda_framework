<?php
/**
 * @author mei
 * @date 2017/6/20
 * @since 2017/6/20 description
 */

require(__DIR__ . '/../vendor/autoload.php');

Tracy\Debugger::enable();
Tracy\Debugger::$strictMode = true;


$data = [
    'disabled'=>'0',
    'uid' =>null
];

echo 'input:'.'<br>';
print '<pre>'.print_r($data, true).'</pre>';

$data = new \ArrayObject($data);

$validator = new JesdaLib\Common\DataValidator('Employee');
$result = $validator->validate($data);
echo 'result and message:'.'<br>';
var_dump($result);
if(!$result){
    print '<pre>'.print_r($validator->getErrorMessages(), true).'</pre>';
}
$data = $data->getArrayCopy();
echo '<br>'.'output data(after filter):'.'<br>';
print '<pre>'.print_r($data, true).'</pre>';
foreach ($data as $key=>$row) {
    echo $key. ' value:'. $row .' type:'. gettype($row).'<br>';
}