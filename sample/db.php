<?php
/**
 * @author mei
 * @date 2017/7/26
 * @since 2017/7/26 多資料庫連線測試
 */
require(__DIR__ . '/../vendor/autoload.php');

include __DIR__ . '/../libraries/JesdaLib/Common/Configuration.php';

Tracy\Debugger::enable();
Tracy\Debugger::$strictMode = true;


$dbConn = \JesdaLib\Common\DbConnector::getDb();
$tableInfo = $dbConn->getTableInfo('acl_item');
var_dump($tableInfo);

$dbConn = \JesdaLib\Common\DbConnector::getDb('db2');
$tableInfo = $dbConn->getTableInfo('country');
var_dump($tableInfo);

$dbConn = \JesdaLib\Common\DbConnector::getDb('db3');
$tableInfo = $dbConn->getTableInfo('employee');
var_dump($tableInfo);