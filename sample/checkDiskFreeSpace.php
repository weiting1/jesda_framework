<?php
/**
 * @author mei
 * @date 2017/8/18
 * @since 2017/8/18 檢查硬碟容量，加入系統排程
 */

require(__DIR__ . '/../vendor/autoload.php');

$disk = IS_WINDOWS? strstr(PROJECT_PATH, ':', true).':':'/';
$arr = [];
$arr['totalBytes'] = disk_total_space($disk);
$arr['totalGB'] = sprintf('%.3f', ($arr['totalBytes']/1024/1024/1024));
$arr['freeBytes'] = disk_free_space($disk);
$arr['freeGB'] = sprintf('%.3f', ($arr['freeBytes']/1024/1024/1024));
echo json_encode($arr, 320);