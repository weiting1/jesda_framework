<?php
/**
 * @author mei
 * @date 2017/6/22
 * @since 2017/6/22 description
 */
require(__DIR__ . '/../vendor/autoload.php');

Tracy\Debugger::enable();
Tracy\Debugger::$strictMode = true;


$configPath = PROJECT_PATH.'/config/application.ini'; # ini檔案路徑
$reader = new JesdaLib\Common\IniReader($configPath);

$config = $reader->getData(null, 'production'); # 所有資料
print '<pre>'.print_r($config, true).'</pre>';
$config = $reader->getData('resources.db', 'production'); # 指定特定路徑資料，輸入字串，使用dot串接
print '<pre>'.print_r($config, true).'</pre>';
$config = $reader->getData(['resources', 'db'], 'development'); # 指定特定路徑資料，輸入陣列
print '<pre>'.print_r($config, true).'</pre>';
$config = $reader->getData('resources.db'); # section可省略，由getenv取得，無則取第一項
print '<pre>'.print_r($config, true).'</pre>';
$config = $reader->getConfig('resources.db'); # alias function
print '<pre>'.print_r($config, true).'</pre>';