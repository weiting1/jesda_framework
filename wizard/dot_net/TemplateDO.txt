using JesdaNet.DataObject;
using System;
using static JesdaNet.DataObject.DBColumnInfo;

/// <summary>
/// 精靈自動產生
/// </summary>
namespace {{project_name}}.Models.DataObject
{
    [DBTableInfo("{{original_table}}")]
    public class {{asp_table}} : BaseDO
    {
        {{column}}
    }
}