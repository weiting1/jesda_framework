<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 2017/6/12
 * Time: 下午4:01
 */

require_once("../../../vendor/autoload.php");

$tableName = "admin";

$db = new \JesdaLib\Common\DBConfiguration();

$conn = $db->getConnection();

$sql = "";
$pKey = "column_name";
if ("mysql" == DB_Server)
{
    $sql = "SHOW KEYS FROM {$tableName} WHERE Key_name = 'PRIMARY'";
    $pKey = "Column_name";
}
if ("pgsql" == DB_Server) {
    $sql = "SELECT k.column_name
FROM information_schema.table_constraints t
  JOIN information_schema.key_column_usage k
  USING(constraint_name,table_schema,table_name)
WHERE t.constraint_type='PRIMARY KEY'
      AND t.table_schema='public'
      AND t.table_name='{$tableName}'";
}

if ("dblib" == DB_Server)
{
    $sql = "SELECT COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'common_mailbox' AND TABLE_SCHEMA='dbo'";
}

if (empty($sql))
    die("還沒實作這個sql server");

$stmt = $conn->prepare($sql);
$stmt->execute();
$row = $stmt->fetch(\PDO::FETCH_OBJ);
$primaryKey = $row->$pKey;

$sql = "SELECT * FROM {$tableName} LIMIT 1";

$stmt = $conn->prepare($sql);
$stmt->execute();
$row = $stmt->fetch(\PDO::FETCH_OBJ);

$do = file_get_contents('./TemplateDO.txt');
$columnResult = "\r\n";

foreach (range(0, $stmt->columnCount() - 1) as $columnIndex) {
    $meta = $stmt->getColumnMeta($columnIndex);

    if ($columnIndex === 0) {
        $tableExplde = explode('_', $meta['table']);
        $tableUCExplode = "";
        foreach($tableExplde as $value)
        {
            $tableUCExplode .= ucfirst($value);
        }
        $tableName = $tableUCExplode;
        $do = str_replace("{{original_table}}", $meta['table'], $do);
        $do = str_replace("{{php_table}}", $tableName . "DO", $do);
    }

    $column = file_get_contents('./ColumnTemplate.txt');
    $type = getMyType(strtolower($meta['native_type']));
    $replace = ['column' => $meta['name'], 'type' => $type];

    if ($primaryKey == $meta['name'])
        $replace['PK'] = true;

    if ($meta['name'] == "id")
        $replace['AUTO'] = true;

    $column = str_replace("{{column_comments}}", json_encode($replace), $column);
    $columnExplode = explode('_', $meta['name']);
    $columnUCExplode = "";
    foreach($columnExplode as $value)
    {
        $columnUCExplode .= ucfirst($value);
    }
    $columnUCExplode = lcfirst($columnUCExplode);
    $column = str_replace("{{php_column}}", $columnUCExplode, $column);
    $columnResult .= $column . "\r\n\r\n";

}

$do = str_replace("{{column}}", $columnResult, $do);
?>
    <textarea style="width: 800px; height: 400px"><?= $do ?></textarea>

<?php

function getMyType($type)
{
    switch ($type)
    {
        case "long":
        case "tiny":
        case "int4":
        case "float8":
            return "INT";

        case "bool":
            return "BOL";

        case "json":
        case "jsonb":
            return "jsonb";

        default:
            return "STR";
    }
}

file_put_contents(__DIR__.'/../../../libraries/Jesda/DataObject/'. $tableName . 'DO.php', $do);