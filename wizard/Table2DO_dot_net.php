<?php
/**
 * 之接產生資料庫中所有表的DO
 * User: mei
 * Date: 2017/10/12
 *
 */

require_once("../vendor/autoload.php");

$tableName = "admin";
$projectName = 'ntu_tu_pe';
$templateFolder = './dot_net';

$iniReader = new \JesdaLib\Common\IniReader();
$dbServer = $iniReader->getConfig('db3.adapter');
$dbName = $iniReader->getConfig('db3.dsn.Database');
$dbConn = \JesdaLib\Common\DbConnector::getDb('db3');
$tableInfo = $dbConn->getTableInfo($tableName);

$projectName = strtoupper($projectName);
$originalTable = strtolower($tableName);
$aspTable = under2Camel($tableName)."DO";
$fileName = $aspTable.'.cs';

$do = file_get_contents($templateFolder.'/TemplateDO.txt');
$columnResult = "\r\n";

foreach ($tableInfo as $columnInfo){
    $columnNameDb = $columnInfo['field'];
    $columnTypeDb = getMyType($columnInfo['type'], $dbServer);
    $isPrimary = ($columnInfo['key'] == 1)? ', true, true':'';
    $columnNameAsp = under2Camel($columnInfo['field']);
    $columnTypeAsp = getAspType($columnInfo['type']);

    $column = file_get_contents($templateFolder.'/ColumnTemplate.txt');
    $from = ['{{column_name_db}}', '{{column_type_db}}', '{{is_primary}}', '{{column_name_asp}}', '{{column_type_asp}}'];
    $to = [$columnNameDb, $columnTypeDb, $isPrimary, $columnNameAsp, $columnTypeAsp];
    $columnResult.= str_replace($from, $to, $column) . "\r\n\r\n";;
}

$from = ['{{project_name}}', '{{original_table}}', '{{asp_table}}', '{{column}}'];
$to = [$projectName, $originalTable, $aspTable, $columnResult];
$do = str_replace($from, $to, $do);
?>
    <textarea style="width: 800px; height: 400px"><?= $do ?></textarea>

<?php

function camel2Under($str)
{
    $str = lcfirst($str);
    $arr = str_split($str);
    $result = '';
    foreach ($arr as $value){
        $result.= ctype_upper($value)? '_'.strtolower($value):$value;
    }
    return $result;
}

function under2Camel($str, $isLowerCaseFirst = false)
{
    $arr = explode('_', $str);
    $result = '';
    foreach ($arr as $value){
        $result.= ucfirst($value);
    }
    return $isLowerCaseFirst? lcfirst($result):$result;
}

function getMyType($type, $dbServer = 'mysql')
{
    $type = strtolower($type);
    if($dbServer == 'pgsql') {
        switch ($type) {
            case "long":
            case "tiny":
            case "int4":
            case "float8":
                return "INT";

            case "bool":
                return "BOL";

            case "json":
            case "jsonb":
                return "jsonb";

            default:
                return "STR";
        }
    } elseif($dbServer == 'mysql'){
        switch ($type) {
            case "int":
            case "tinyint":
            case "smallint":
            case "float":
            case "decimal":
                return "INT";

            case "bool":
                return "BOL";

            case "json":
                return "jsonb";

            default:
                return "STR";
        }
    } elseif($dbServer == 'sqlsrv'){
        switch ($type) {
            case "int":
            case "tinyint":
            case "smallint":
            case "bigint":
            case "float":
            case "decimal":
                return "INT";

            case "datetime":
            case "datetime2":
                return "TIME";

            case "varchar":
            case "nvarchar":
            case "char":
            case "nchar":
            case "text":
            case "ntext":
                return "STR";

            default:
                return "OBJECT";
        }
    }
    die('尚未實做'.$dbServer);
}

function getAspType($type)
{
    switch ($type) {
        case "int":
        case "tinyint":
        case "smallint":
        case "bigint":
        case "float":
        case "decimal":
            return "int";

        case "datetime":
        case "datetime2":
            return "DateTime";

        default:
            return "string";
    }
}

#file_put_contents(__DIR__.'/../../../libraries/Jesda/DataObject/'. $tableName . 'DO.php', $do);