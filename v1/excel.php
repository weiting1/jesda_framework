<?php
/**
 * @author mei
 * @date 2017/8/6
 * @since 2017/8/6 description
 */

require(__DIR__ . '/../vendor/autoload.php');
include PROJECT_PATH.'/data/enums/main.php';

use \JesdaLib\API\ExcelAPI;

$clientAccess = new ExcelAPI();
$clientAccess->inputs();