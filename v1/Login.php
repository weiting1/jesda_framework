<?php 

require(__DIR__ . '/../vendor/autoload.php');

use JesdaLib\API\FrontLoginAPI;
use JesdaLib\API\ApplyStuFormAPI;
use JesdaLib\API\SystemLogAPI;

$clientAccess = new FrontLoginAPI(new SystemLogAPI(new ApplyStuFormAPI()));
$clientAccess->inputs();