<?php 

require(__DIR__ . '/../vendor/autoload.php');

use JesdaLib\API\SessionVerifyAPI;
use JesdaLib\API\FileUploadAPI;
use JesdaLib\API\ApplyStuFormAPI;

$clientAccess = new SessionVerifyAPI(new ApplyStuFormAPI(new FileUploadAPI()));
$clientAccess->inputs();