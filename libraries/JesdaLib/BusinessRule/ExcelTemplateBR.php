<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 2017/6/12
 * Time: 上午11:20
 */

namespace JesdaLib\BusinessRule;

use JesdaLib\Common\DBConfiguration;
use JesdaLib\Common\LogHelper;

class ExcelTemplateBR extends BaseBR
{
    private $logHelper;

    public function __construct($logHelper = null)
    {
        $this->logHelper = is_null($logHelper) ? new LogHelper(get_class($this)) : $logHelper;
    }

    /**
     * @return array|null
     */
    public function getAllTemplate()
    {
        $db = new DBConfiguration();
        //自訂SQL的寫法
        $sql = "SELECT * FROM common_excel_template";

        $stmt = $db->getSQLStatement($sql);
        $stmt->execute();

        $returnArray = $stmt->fetchAll();
        $db->closeConnection();

        if ($stmt->errorCode() != "0000") {
            $this->logHelper->error("get excel template fail", $stmt->errorInfo());
            return null;
        }
        else {
            return $returnArray;
        }
    }

    public function saveTemplate($api, $name, $columns)
    {
        $db = new DBConfiguration();

        $sql = "DELETE FROM common_excel_template WHERE template_name = :template_name and api = :api";
        $stmt = $db->getSQLStatement($sql);
        $stmt->bindValue(":template_name", $name);
        $stmt->bindValue(":api", $api);
        $stmt->execute();

        if ($stmt->errorCode() != "0000") {
            $this->logHelper->error("delete excel template fail", $stmt->errorInfo());
            $db->closeConnection();
            return false;
        }
        else {
            $sql = "INSERT INTO common_excel_template VALUES(:api, :template_name, :columns, :updatedBy, :updatedAt)";
            $stmt = $db->getSQLStatement($sql);
            $stmt->bindValue(":api", $api);
            $stmt->bindValue(":template_name", $name);
            $stmt->bindValue(":columns", $columns);
            $stmt->bindValue(":updatedBy", $GLOBALS['USER']);
            $stmt->bindValue(":updatedAt", date("Y-m-d H:i:s"));
            $stmt->execute();

            if ($stmt->errorCode() != "0000") {
                $this->logHelper->error("insert excel template fail", $stmt->errorInfo());
                $db->closeConnection();
                return false;
            }
            else {
                $db->closeConnection();
                return true;
            }
        }
    }
}