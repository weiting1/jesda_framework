<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 2017/8/3
 * Time: 下午4:22
 */

namespace JesdaLib\BusinessRule;


use JesdaLib\Common\DBConfiguration;
use JesdaLib\Common\LogHelper;

class PasswordBR extends BaseBR
{
    private $logHelper;

    public function __construct($logHelper = null)
    {
        $this->logHelper = is_null($logHelper) ? new LogHelper(get_class($this)) : $logHelper;
    }

    /**
     * @param $jwt
     * @param $password
     * @return bool
     */
    public function resetPasswordByJWT($jwt, $password)
    {
        $payload = "";
        $result = false;
        
        if ($this->decodeJWTForPayload($jwt, $payload, Secret)) {
            $hashedPassword = $this->sha256($password, Secret);
            $payload = json_decode($payload, false);
            
            $tableName = $payload->tableName;
            $emailColumnName = $payload->emailColumnName;
            $passwordColumnName = $payload->passwordColumnName;
            
            $timestamp = $payload->timestamp;
            $email = $payload->email;

            if (strtotime(date("Y-m-d H:i:s")) - $timestamp < 30 * 60) {
                $sql = "UPDATE $tableName SET $passwordColumnName = :pwd, jwt = '' WHERE $emailColumnName = :email and jwt = :jwt";

                $db = new DBConfiguration();

                $stmt = $db->getSQLStatement($sql);
                $stmt->bindParam(":email", $email);
                $stmt->bindParam(":jwt", $jwt);
                $stmt->bindParam(":pwd", $hashedPassword);

                $stmt->execute();
                if ($stmt->errorCode() != "0000") {
                    $this->logHelper->error("RESET PASSWORD FAIL: " . $tableName, $db->errorMessage);
                } else {
                    if ($stmt->rowCount() === 1) {
                        $result = true;
                    }
                    else {
                        $this->logHelper->warn("RESET PASSWORD Count: " . $stmt->rowCount(), [$email, $jwt]);
                    }
                }
                $db->closeConnection();
            }
            else
            {
                $this->logHelper->warn("RESET PASSWORD JWT TIMEOUT: ", $payload);
            }
        }
        else
        {
            $this->logHelper->warn("RESET PASSWORD JWT DECODE FAIL: ", $jwt);
        }

        return $result;
    }

    /**
     * @param $email
     * @param $tableName
     * @param $emailColumnName
     * @param $passwordColumnName
     * @return null|string
     */
    public function getJWTtoResetPassword($email, $tableName, $emailColumnName, $passwordColumnName)
    {
        $db = new DBConfiguration();

        $sql = "SELECT $emailColumnName FROM $tableName WHERE $emailColumnName = :email";
        $stmt = $db->getSQLStatement($sql);
        $stmt->bindParam(":email", $email);

        $stmt->execute();
        $jwt = null;
        if ($stmt->errorCode() != "0000")
        {
            $this->logHelper->error("GET EMAIL FAIL: ".$tableName, $db->errorMessage);
        }
        else
        {
            if ($row = $stmt->fetch(\PDO::FETCH_OBJ))
            {
                //gen jwt
                $payload = ["timestamp" => strtotime(date("Y-m-d H:i:s")),
                            "tableName" => $tableName,
                            "emailColumnName" => $emailColumnName,
                            "passwordColumnName" => $passwordColumnName,
                            "email" => $email];
                $jwt = $this->generateJWT(json_encode($payload), Secret);

                $sql = "UPDATE $tableName SET jwt = :jwt WHERE $emailColumnName = :email";
                $stmt = $db->getSQLStatement($sql);
                $stmt->bindParam(":jwt", $jwt);
                $stmt->bindParam(":email", $email);

                $stmt->execute();
                if ($stmt->errorCode() != "0000")
                {
                    $this->logHelper->error("Update JWT FAIL: ".$tableName, $db->errorMessage);
                }
            }
            else
            {
                $this->logHelper->error("CANT FIND EMAIL: ", $email);
            }
        }
        $db->closeConnection();
        return $jwt;
    }
}