<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 2017/6/12
 * Time: 上午11:20
 */

namespace JesdaLib\BusinessRule;

use JesdaLib\Common\DBConfiguration;
use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\CommonEmailTemplateDO;

class EmailTemplateBR extends BaseBR
{
    private $logHelper;

    public function __construct($logHelper = null)
    {
        $this->logHelper = is_null($logHelper) ? new LogHelper(get_class($this)) : $logHelper;
    }

    /**
     * @return CommonEmailTemplateDO[]
     */
    public function getAllTemplate($templateId)
    {
        $db = new DBConfiguration();
        //自訂SQL的寫法
        $sql = "SELECT * FROM common_email_template where template_id = :template_id";

        $stmt = $db->getSQLStatement($sql);
        $stmt->bindValue(":template_id", $templateId);
        $stmt->execute();

        $returnArray = [];
        while($row = $stmt->fetch(\PDO::FETCH_OBJ))
        {
            $do = new CommonEmailTemplateDO();
            $do->setFromDataRow($row);

            array_push($returnArray, $do);
        }
        $db->closeConnection();

        if ($stmt->errorCode() != "0000") {
            $this->logHelper->error("get email template fail", $stmt->errorInfo());
            return null;
        } else {
            return $returnArray;
        }
    }

    /**
     * @param $templateId
     * @return CommonEmailTemplateDO|null
     */
    public function getTemplateByTemplateId($templateId)
    {
        $db = new DBConfiguration();
        //自訂SQL的寫法
        $sql = "SELECT * FROM common_email_template WHERE template_id = :id";

        $stmt = $db->getSQLStatement($sql);
        $stmt->bindValue(":id", $templateId);
        $stmt->execute();

        $returnObject = null;
        if ($row = $stmt->fetch(\PDO::FETCH_OBJ))
        {
            $returnObject = new CommonEmailTemplateDO();
            $returnObject->setFromDataRow($row);
        }
        $db->closeConnection();
        return $returnObject;
    }

    /**
     * 預設email template 應該是固定的
     * @param $id
     * @param $title
     * @param $content
     * @return bool
     */
    public function saveTemplate($id, $title, $content, $templateName, $templateId)
    {
        $db = new DBConfiguration();

        $do = new CommonEmailTemplateDO();
        $do->id = $id;
        $do->title = $title;
        $do->templateName = $templateName;
        $do->content = $content;
        $do->locale = "CH";
        $do->templateId = $templateId;

        if (empty($do->id))
        {
            $db->new($do);
        }
        else
        {
            $db->modify($do);
        }

        if (!empty($db->errorMessage)) {
            $this->logHelper->error("update email template fail", $db->errorMessage);
            $db->closeConnection();
            return false;
        } else {
            $db->closeConnection();
            return true;
        }
    }

    public function deleteTemplate($id)
    {
        $db = new DBConfiguration();

        $do = new CommonEmailTemplateDO();
        $do->id = $id;
        $db->destroy($do);

        $db->closeConnection();

        if (!empty($db->errorMessage)) {
            $this->logHelper->error("update email template fail", $db->errorMessage);
            $db->closeConnection();
            return false;
        } else {
            $db->closeConnection();
            return true;
        }
    }

}