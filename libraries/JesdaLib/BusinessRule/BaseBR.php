<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 12/22/16
 * Time: 14:11
 */

namespace JesdaLib\BusinessRule;

class BaseBR
{
    const JWT_HEADER = '{ "alg": "HS256", "typ": "JWT" }';

    public function generateJWT($payload, $secret)
    {
        $segments = [];
        $segments[] = $this->URLSafeB64Encode(BaseBR::JWT_HEADER);
        $segments[] = $this->URLSafeB64Encode($payload);
        $payloadInput = implode('.', $segments);
        $signature = hash_hmac('sha256', $payloadInput, $secret, true);
        $segments[] = $this->URLSafeB64Encode($signature);
        return implode('.', $segments);
    }

    private function URLSafeB64Encode($input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    public function decodeJWTForPayload($jwt, &$payload, $secret)
    {
        $split = explode(".", $jwt);
        $hashed = hash_hmac('sha256', $split[0] . '.' . $split[1], $secret, true);
        $base64Hashed = rtrim(strtr(base64_encode($hashed), '+/', '-_'), '=');

        if ($base64Hashed == $split[2])
        {
            $payload = base64_decode($split[1]);
            return true;
        }
        else
        {
            $payload = "";
            return false;
        }
    }
    
    public function sha256($original, $secret)
    {
        return hash_hmac('sha256', $original, $secret, false);
    }
}