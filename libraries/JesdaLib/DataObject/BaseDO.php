<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 2017/6/12
 * Time: 下午3:59
 */

namespace JesdaLib\DataObject;


class BaseDO
{
    public function castAs($newClass)
    {
        $obj = new $newClass;
        foreach (get_object_vars($this) as $key => $name) {
            $obj->$key = $name;
        }
        return $obj;
    }

    //Mapping a JSON object to Data Object
    public function set($json)
    {
        if (is_array($json)) {
            return $this->setFromArray($json);
        } else {
            if (is_object($json)) {
                $json = json_encode($json, 522);
            }
            $data = json_decode($json, true);
            if (is_array($data) && !is_null($data)) {
                foreach ($data AS $key => $value)
                    $this->{$key} = is_array($value) ? (object)$value : $value;

                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * @param array $array
     * @return bool
     */
    public function setFromArray(Array $array)
    {
        if (is_array($array) && !is_null($array)) {
            foreach ($array AS $key => $value) $this->{$key} = $value;

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param \ArrayObject $arrayObject
     * @return bool
     */
    public function setFromArrayObject(\ArrayObject $arrayObject)
    {
        return $this->setFromArray($arrayObject->getArrayCopy());
    }

    /**
     * @param $row
     */
    public function setFromDataRow($row)
    {
        if (!empty($row)) {
            $class = new \ReflectionClass($this);
            $properties = $class->getProperties();
            foreach ($properties as $property) {
                $attribute = $this->getAttributeObj($property);
                $column = $row->{strtolower($attribute['column'])};
                if (!is_null($column)) {
                    if ($attribute["type"] === "jsonb")
                        $this->{$property->name} = json_decode($column);
                    else if ($attribute["type"] === "INT")
                        $this->{$property->name} = (int)$column;
                    else
                        $this->{$property->name} = $column;
                }
            }

        }
    }

    /**
     * @param \ReflectionProperty $attributeOfDO
     * @return array
     */
    private function getAttributeObj(\ReflectionProperty $attributeOfDO)
    {
        $comment = $attributeOfDO->getDocComment();
        preg_match('/\{(.|\n)*\}/', $comment, $match);
        return json_decode($match[0], true);
    }
}
