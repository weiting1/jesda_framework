<?php

/* 精靈自動產生 */
namespace JesdaLib\DataObject;

/**
{"tableName":"common_mailbox"}
 */
class CommonMailboxDO extends BaseDO
{

    
    /**
    {"column":"id","type":"INT","PK":true,"AUTO":true}
     */
    public $id;

    /**
    {"column":"sender","type":"STR"}
     */
    public $sender;

    /**
    {"column":"receiver","type":"STR"}
     */
    public $receiver;

    /**
    {"column":"title","type":"STR"}
     */
    public $title;

    /**
    {"column":"content","type":"STR"}
     */
    public $content;

    /**
    {"column":"updated_by","type":"STR"}
     */
    public $updatedBy;

    /**
    {"column":"updated_at","type":"STR"}
     */
    public $updatedAt;



}