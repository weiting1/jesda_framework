<?php

/* 精靈自動產生 */
namespace JesdaLib\DataObject;


/**
{"tableName":"common_email_template"}
 */
class CommonEmailTemplateDO extends BaseDO
{


    /**
    {"column":"id","type":"STR","PK":true}
     */
    public $id;

    /**
    {"column":"title","type":"STR"}
     */
    public $title;

    /**
     * {"column":"template_id", "type":"STR"}
     */
    public $templateId;

    /**
    {"column":"description","type":"STR"}
     */
    public $templateName;

    /**
    {"column":"content","type":"STR"}
     */
    public $content;

    /**
    {"column":"locale","type":"STR"}
     */
    public $locale;

    /**
    {"column":"updated_by","type":"STR"}
     */
    public $updatedBy;

    /**
    {"column":"updated_at","type":"STR"}
     */
    public $updatedAt;



}