<?php


namespace JesdaLib\DataObject;

class LibStatusDO {

    const OK = 200;

    //系統層級的錯誤
    const Session_Invalid = 401;
    const API_Access_Denied = 402;
    const Login_Fail = 403;
    const ExcelTemplate_Fail = 404;
    const ExcelTemplate_Columns_Should_be_Array_Fail = 405;
    const Upload_File_Fail = 406;
    const Change_Password_Request_Data_Empty = 407;
    const Reset_Password_Data_Empty = 408;
    const Reset_Password_Fail = 409;
    const Reset_Password_Fail_Cant_Find_Email = 410;
    const EmailTemplate_Fail = 411;
    const EmailNotification_Data_Empty = 412;
    const EmailNotification_Sent_Fail = 413;
    const Update_SEQ_Fail = 414;
    const Update_SEQ_Empty = 415;
    const Update_Columns_Fail = 416;

    const Validation_Wizard = 500;

    public function getStatus($statusCode)
    {
        return json_encode(array('statusCode' => $statusCode, 'statusMessage' => $this->getStatusMessage($statusCode)));
    }

    public function getStatusMessage($statusCode)
    {
        $class = new \ReflectionClass($this);
        $constants = array_flip($class->getConstants());

        return $constants[$statusCode];
    }

}
