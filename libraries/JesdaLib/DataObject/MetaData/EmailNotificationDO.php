<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 2017/9/21
 * Time: 上午11:55
 */

namespace JesdaLib\DataObject\MetaData;

class EmailNotificationDO
{
    public $metadata = ['templateId' => '', 'replaceContent' => [], 'toAddress' => ''];

    public function setTemplateId($templateId)
    {
        $this->metadata['templateId'] = $templateId;
    }

    public function setReplaceContent(Array $replaceContent)
    {
        $this->metadata['replaceContent'] = $replaceContent;
    }

    public function setToAddress($toAddress)
    {
        $this->metadata['toAddress'] = $toAddress;
    }

    public function setToCC($toCC)
    {
        $this->metadata['toCC'] = $toCC;
    }
}