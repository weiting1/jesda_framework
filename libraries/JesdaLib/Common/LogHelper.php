<?php
/**
 * Created by PhpStorm.
 * User: Kurt
 * Date: 12/20/16
 */

namespace JesdaLib\Common;

use Monolog\Handler\SlackHandler;
use Monolog\Logger;

class LogHelper
{
    private $warningLog;
    private $errorLog;
    private $infoLog;

    public $open = [
        'PDOHandler'        => true,
        'JesdaSlackHandler' => false,
        'FileLogHandler'    => false,
    ];

    public function __construct($objectName)
    {
        date_default_timezone_set("Asia/Taipei");

        $fix = "_";
        if (strpos($objectName, 'API') !== false) {
            $fix = "_API_";
        }
        else if (strpos($objectName, 'BR') !== false) {
            $fix = "_BR_";
        }

        $format = "[%datetime%] %channel%\n %message%\n\n %context%\n\n";
        // create a log channel


        $this->warningLog = new Logger($objectName);
        $this->errorLog = new Logger($objectName);
        $this->infoLog = new Logger($objectName);

        if($this->open['PDOHandler']) {
            $this->warningLog->pushHandler(new PDOHandler(Logger::WARNING));
            $this->errorLog->pushHandler(new PDOHandler(Logger::ERROR));
            $this->infoLog->pushHandler(new PDOHandler(Logger::INFO));
        }
//        $this->warningLog->pushHandler(new JesdaSlackHandler(Logger::WARNING));

        // if($this->open['JesdaSlackHandler']) {
        //     $this->errorLog->pushHandler(new JesdaSlackHandler(Logger::ERROR));
        // }

        if($this->open['FileLogHandler']) {
            $this->warningLog->pushHandler(new FileLogHandler(Logger::WARNING));
            $this->errorLog->pushHandler(new FileLogHandler(Logger::ERROR));
            $this->infoLog->pushHandler(new FileLogHandler(Logger::INFO));
        }

    }
    public function info($msg, $relatedDO = array())
    {
        $this->infoLog->addInfo($msg, (array) $relatedDO);
    }

    public function warn($msg, $relatedDO = array())
    {
        $this->warningLog->addWarning($msg, (array) $relatedDO);
    }

    public function error($msg, $relatedDO = array())
    {
        $this->errorLog->addError($msg, (array) $relatedDO);
    }
}
