<?php
/**
 * @author mei
 * @date 2017/6/21
 * @since 2017/6/21
 * 讀取ini設定檔工具
 *
 * ini檔案區段可以使用「:」指示資料繼承，例如：
 * [production]
 * a=1
 * b=1
 *
 * [development:production]
 * b=2
 *
 * 取出development區段結果:
 * a=1
 * b=2
 *
 * 使用方法
 * $configPath = PROJECT_PATH.'/config/application.ini'; # ini檔案路徑
 * $reader = new Jesda\Common\IniReader($configPath);
 *
 * $config = $reader->getData(null, 'production'); # 所有資料
 * print '<pre>'.print_r($config, true).'</pre>';
 * $config = $reader->getData('resources.db', 'production'); # 指定特定路徑資料，輸入字串，使用dot串接
 * print '<pre>'.print_r($config, true).'</pre>';
 * $config = $reader->getData(['resources', 'db'], 'development'); # 指定特定路徑資料，輸入陣列
 * print '<pre>'.print_r($config, true).'</pre>';
 * $config = $reader->getData('resources.db'); # section可省略，由getenv取得，無則取第一項
 * print '<pre>'.print_r($config, true).'</pre>';
 * $config = $reader->getConfig('resources.db'); # alias function
 * print '<pre>'.print_r($config, true).'</pre>';
 */
namespace JesdaLib\Common;

class IniReader
{
    /**
     * 讀取設定擋路徑
     * @var string
     */
    protected $filePath = INI_FILE_PATH;

    /**
     * 原始資料
     * @var array
     */
    protected $rawData;

    /**
     * 處理繼承後資料
     * @var array
     */
    protected $data;

    public function __construct($filePath = null)
    {
        if(null === $filePath){
            $filePath = $this->filePath;
        }
        $this->setFilePath($filePath);
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * 設定檔案路徑
     * @param $filePath
     * @return $this
     * @throws \Exception
     */
    public function setFilePath($filePath)
    {
        if(!file_exists($filePath)){
            throw new \Exception('ini file does not exists');
        }

        $this->filePath = $filePath;
        $this->setup();
        return $this;
    }

    /**
     * 讀取設定檔案
     * @param bool $isUpdate
     * @return array
     */
    public function getRawData($isUpdate = false)
    {
        if($isUpdate || null === $this->rawData) {
            $filePath = $this->getFilePath();
            $data = parse_ini_file($filePath, true);
            $rawData = [];
            foreach ($data as $section => $sectionData){
                $rawData[$section] = $this->formatSectionData($sectionData);
            }

            $this->rawData = $rawData;
        }
        return $this->rawData;
    }

    /**
     * 轉換.成陣列階層
     * @param $sectionData
     * @return array
     */
    protected function formatSectionData($sectionData)
    {
        $formatData = [];
        foreach ($sectionData as $key => $value) {
            $layers = explode('.', $key);
            $result = [$key=>$value];
            if(count($layers) > 1) {
                $layers = array_reverse($layers);
                $result = $value;
                foreach ($layers as $layer){
                    $result = [$layer => $result];
                }
            }

            $formatData = array_replace_recursive($formatData, $result);
        }
        return $formatData;
    }

    /**
     * alias of getData($subPaths = null, $section = null)
     * @param null $subPaths 子路徑
     * @param null $section 指定區段
     * @return array|mixed
     */
    public function getConfig($subPaths = null, $section = null)
    {
        return $this->getData($subPaths, $section);
    }

    /**
     * 取得繼承後區段資料
     * @param null|string|array $subPaths 子路徑
     * @param null|string $section 指定區段
     * @return array|mixed
     * @throws \Exception
     */
    public function getData($subPaths = null, $section = null)
    {
        # 由APPLICATION_ENV取得 / 取第一項
        if(!is_string($section) || empty($section)) {
            $section = getenv('APPLICATION_ENV');
            if(!$section) {
                $keys = array_keys($this->data);
                $section = reset($keys);
            }
        }

        if(!isset($this->data[$section])) {
            throw new \Exception('section ini not found');
        }

        $returnData = $this->data[$section];

        # 取得子路徑
        if(!empty($subPaths)) {

            if(is_string($subPaths)) {
                # 前後移掉
                $subPaths = trim($subPaths, '.');
                if (false !== strpos($subPaths, '.')) {
                    $subPaths = explode('.', $subPaths);
                } else {
                    $subPaths = [$subPaths];
                }
            } elseif(!is_array($subPaths)) {
                throw new \Exception('Invalid type. expected string or array');
            }

            foreach ($subPaths as $subPath) {
                if(isset($returnData[$subPath])) {
                    $returnData = $returnData[$subPath];
                } else {
                    $returnData = null;
                    break;
                }
            }
        }

        return $returnData;
    }

    /**
     * 處理設定檔繼承
     * @throws \Exception
     */
    public function setup()
    {
        $rawData = $this->getRawData(true);

        $sections = array_keys($rawData);

        $data = [];
        # 繼承區段mapping
        $extendSections = [];

        # 整理繼承規則
        foreach ($sections as $section) {
            $section = trim($section);
            $pos = strpos($section, ':');
            $length = strlen($section);
            $count = substr_count($section, ':');

            if($pos === 0 || $pos == $length-1){
                throw new \Exception('「:」不能放在區段名稱前後端');
            }

            if($count > 1){
                throw new \Exception('「:」每個區段只能顯示一次');
            }

            if(strpos($section, ':') !== false){
                $sectionParts = explode(':', $section);
                $extendSections[trim($sectionParts[0])] = trim($sectionParts[1]);
                $data[trim($sectionParts[0])] = $rawData[$section];
            } else {
                $data[$section] = $rawData[$section];
            }
        }

        # 處理繼承
        $hasUpdate = true;
        while( $hasUpdate && count($extendSections) > 0 ){
            $hasUpdate = false;
            foreach ($extendSections as $section => $extendSection) {
                if (array_key_exists($extendSection, $data)) {
                    $data[$section] = array_replace_recursive($data[$extendSection], $data[$section]);
                    unset($extendSections[$section]);
                    $hasUpdate = true;
                }
            }
        }

        $this->data = $data;
    }
}