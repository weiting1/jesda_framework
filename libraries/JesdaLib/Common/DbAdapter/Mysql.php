<?php

namespace JesdaLib\Common\DbAdapter;

/**
 * @author mei
 * @date 2017/7/26
 * @since 2017/7/26 mysql PDO handler
 */
class Mysql  extends \pdo implements DbAdapter
{

    public function getTableInfo($tableName)
    {
        if(preg_match('/^[a-zA-Z0-9_]+$/', $tableName) !== 1) {
            return false;
        }

        $stmt = $this->prepare("SHOW FULL FIELDS FROM `{$tableName}`");
        if(!$stmt->execute()){
            return false;
        }
        return $stmt->fetchAll();
    }

    public function showTables()
    {
        $stmt = $this->prepare("SHOW TABLES");

        if(!$stmt->execute()){
            return false;
        }
        return $stmt->fetchAll();
    }
}