<?php
namespace JesdaLib\Common\DbAdapter;

/**
 * @author mei
 * @date 2017/10/13
 * @since 2017/10/13 description
 */

interface DbAdapter
{
    public function getTableInfo($tableName);

    public function showTables();
}
