<?php

namespace JesdaLib\Common\DbAdapter;

/**
 * @author mei
 * @date 2017/7/26
 * @since 2017/7/26 Sqlsrv PDO handler
 */
class Sqlsrv extends \pdo implements DbAdapter
{
    public function getTableInfo($tableName)
    {
        $query = "select 
            sc.name as [field],
            ic.data_type as [type],
            ic.collation_name as [collation],
            ic.is_nullable as [null],
            ic.column_default as [default],
            ik.is_primary_key as [key],
            sep.value [comment]
        from sys.tables st
        inner join sys.columns sc on st.object_id = sc.object_id
        left join sys.extended_properties sep on st.object_id = sep.major_id
                                             and sc.column_id = sep.minor_id
                                             and sep.name = 'MS_Description'
        inner join INFORMATION_SCHEMA.COLUMNS ic on ic.COLUMN_NAME = sc.name and ic.TABLE_NAME = st.name
        left join sys.index_columns ics on sc.column_id = ics.column_id and sc.object_id = ics.object_id
        left join sys.indexes ik on ik.object_id = sc.object_id and ics.index_id=ik.index_id
        where st.name = ?";
        
        $stmt = $this->prepare($query);
        if(!$stmt->execute([$tableName])){
            return false;
        }
        return $stmt->fetchAll();
    }

    public function showTables()
    {
        $stmt = $this->prepare("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' ORDER BY TABLE_NAME");
        if(!$stmt->execute()){
            return false;
        }
        return $stmt->fetchAll();
    }
}