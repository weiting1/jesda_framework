<?php

namespace JesdaLib\Common;

use Monolog\Handler\AbstractProcessingHandler;

class JesdaSlackHandler extends AbstractProcessingHandler
{
    public function __construct($level, $bubble = true)
    {
        parent::__construct($level, $bubble);
    }

    /**
     * @param array $record
     */
    public function write(array $record)
    {
        $msg = 'channel: '.$record['channel'] . "\r\n".
            'level: '.$record['level_name']."\r\n".
            'message: '.$record['message']."\r\n".
            'context: '.json_encode($record['context'], 320)."\r\n";

        $payload = ['username' => slackName, 'text' => $msg];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, slackWebhook);
        curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //不要自動印出ok
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['payload' => json_encode($payload, 320)]));
        curl_exec($ch);
        curl_close($ch);
    }

    /**
     *
     */
    private function initialize()
    {

    }
}