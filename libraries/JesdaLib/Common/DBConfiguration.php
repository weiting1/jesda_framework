<?php

namespace JesdaLib\Common;

use JesdaLib\DataObject\BaseDO;

class DBConfiguration
{
    public $errorMessage;
    public $PDOTypeArray = ["INT"=>\PDO::PARAM_INT, "STR"=>\PDO::PARAM_STR,
        "BOL"=>\PDO::PARAM_BOOL, "jsonb"=>\PDO::PARAM_STR, "array"=>\PDO::PARAM_STR];

    /**
     * @var \pdo
     */
    private $conn;

    public function closeConnection()
    {
        $this->conn = null;
    }

    /**
     * @return null|\pdo
     */
    public function getConnection()
    {
        if (is_null($this->conn)) {
            $log = new LogHelper("DB Connection");
            try {
                $options = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4');
                if (DB_Server === "dblib")
                    $this->conn = new \PDO(DB_Server.":charset=UTF-8;host=".DB_Server_Name.":" . DB_Port.";dbname=".DB_DBName, DB_User_Name, DB_Password);
                else if (DB_Server === "sqlsrv")
                    $this->conn = new \PDO(DB_Server.":Server=".DB_Server_Name.",". DB_Port.";Database=".DB_DBName, DB_User_Name , DB_Password);
                else
                    $this->conn = new \PDO(DB_Server.":host=".DB_Server_Name.";port=".DB_Port.";dbname=".DB_DBName, DB_User_Name, DB_Password, $options);
                if (!empty($this->conn->errorCode()) && $this->conn->errorCode() != "00000") {
                    $log->error("errorInfo", $this->conn->errorInfo());
                    $log->error("errorCode", $this->conn->errorCode());
                }

                return $this->conn;
            } catch (\PDOException $e) {
                $log->error("Exception when create connection: " . $e->getMessage(), $e);
                return null;
            }
        }
        else
        {
            return $this->conn;
        }
    }

    public function getSQLStatement($sql)
    {
        if (is_null($this->conn))
            $this->getConnection();

        return $this->conn->prepare($sql);
    }

    public function new(BaseDO $baseDO)
    {
        $class     = new \ReflectionClass($baseDO);
        $tableName = ($this->getTableObj($class))["tableName"];

        if (empty($tableName))
            return null;

        $properties = $class->getProperties();

        $columnArray = array();
        $bindArray = array();
        $hasSEQ = false;

        foreach ($properties as $property)
        {
            if (is_object($baseDO->{$property->name}) || is_array($baseDO->{$property->name}))
            {
                $baseDO->{$property->name} = json_encode($baseDO->{$property->name}, JSON_UNESCAPED_UNICODE);
            }
            $attribute = $this->getAttributeObj($property);
            //var_dump($attribute);
            //can't insert a column setup by auto increment
            if (!is_null($baseDO->{$property->name}) && !is_null($attribute) && empty($attribute["AUTO"])) {
                array_push($columnArray, $attribute["column"]);
                array_push($bindArray, ":".$attribute["column"]);

            }

            if ($property->name == "seq")
                $hasSEQ = true;
        }

        $columnCat = implode(', ', $columnArray);
        $bindCat = implode(', ', $bindArray);

        $sql = "INSERT INTO {$tableName} ({$columnCat}) VALUES ({$bindCat})";
        //var_dump($sql);
        try {
            $conn = $this->getConnection();

            $stmt = $conn->prepare($sql);
            foreach ($properties as $property) {
                $attribute = $this->getAttributeObj($property);
                if (!is_null($baseDO->{$property->name}) && !is_null($attribute) && empty($attribute["AUTO"])) {
                    $stmt->bindValue(":" . $attribute["column"], $baseDO->{$property->name}, $this->PDOTypeArray[$attribute["type"]]);
                }
            }
            $stmt->execute();
            $result = $stmt->rowCount() == 1;
            $this->errorMessage = $stmt->errorInfo()[2];

            if ($result && $hasSEQ) {
                $sql = "UPDATE {$tableName} SET seq = id where seq is null";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }

        }
        catch (\PDOException $exception)
        {
            $this->errorMessage = $exception->getMessage();
            $result = false;
        }

        return $result;

    }

    /**
     * @param \ReflectionClass $classOfDO
     * @return array
     */
    private function getTableObj(\ReflectionClass $classOfDO)
    {
        $comment = $classOfDO->getDocComment();
        preg_match('/\{(.|\n)*\}/', $comment, $match);
        return json_decode($match[0], true);
    }

    /**
     * @param \ReflectionProperty $attributeOfDO
     * @return array
     */
    private function getAttributeObj(\ReflectionProperty $attributeOfDO)
    {
        $comment = $attributeOfDO->getDocComment();
        preg_match('/\{(.|\n)*\}/', $comment, $match);
        return json_decode($match[0], true);
    }

    public function modifyForm(BaseDO $baseDO, $formData, $formType)
    {
        if (!is_null($formData) && !is_null($formType))
        {
            $formDataJSON = json_decode($formData);

            $newFormDO = [];
            foreach ($formDataJSON as $key => $inner)
            {
                if (is_object($inner))
                {
                    if (!empty($baseDO->{$formType}->{$key}))
                    {
                        $newFormDO[$key] = $baseDO->{$formType}->{$key};
                        foreach ($inner as $innerDOKey => $innerDO)
                        {
                            $newFormDO[$key]->{$innerDOKey} = $innerDO;
                        }
                    }
                }
                else
                {
                    $baseDO->{$formType}->{$key} = $inner;
                }
            }
            if (sizeof($newFormDO) > 0)
                $baseDO->{$formType} = json_encode($newFormDO, JSON_UNESCAPED_UNICODE);

            return $this->modify($baseDO);
        }
        else
        {
            return false;
        }
    }

    public function modify(BaseDO $baseDO)
    {
        $class     = new \ReflectionClass($baseDO);
        $tableName = ($this->getTableObj($class))["tableName"];

        if (empty($tableName))
            return null;

        $properties = $class->getProperties();

        $columnArray = array();
        $primaryKeyArray = array();
        foreach ($properties as $property)
        {
            if (is_object($baseDO->{$property->name}) || is_array($baseDO->{$property->name}))
            {
                $baseDO->{$property->name} = json_encode($baseDO->{$property->name}, JSON_UNESCAPED_UNICODE);
            }

            $attribute = $this->getAttributeObj($property);
            //Only those properties 1. value is not null, 2. have attribute, and 3. not PK can be update
            if (!is_null($baseDO->{$property->name}) && !is_null($attribute) && empty($attribute["PK"])) {
                array_push($columnArray, $attribute["column"]." = :".$attribute["column"]);
            }

            if ($attribute["PK"])
            {
                array_push($primaryKeyArray, $attribute["column"]." = :".$attribute["column"]);
            }
        }

        $columnCat = implode(', ', $columnArray);
        $primaryKeyCat = implode(' AND ', $primaryKeyArray);

        $sql = "UPDATE {$tableName} SET {$columnCat} WHERE {$primaryKeyCat}";

        try {
            $conn = $this->getConnection();

            $stmt = $conn->prepare($sql);
            foreach ($properties as $property) {
                $attribute = $this->getAttributeObj($property);
                if (!is_null($baseDO->{$property->name}) && !is_null($attribute)) {
                    $stmt->bindValue(":" . $attribute["column"], $baseDO->{$property->name}, $this->PDOTypeArray[$attribute["type"]]);
                }
            }
            $stmt->execute();
            $result = $stmt->rowCount() >= 1;
            $this->errorMessage = $stmt->errorInfo()[2];
        }
        catch (\PDOException $exception)
        {
            $this->errorMessage = $exception->getMessage();
            $result = false;
        }

        return $result;

    }

    public function destroy(BaseDO $baseDO) {
        $class     = new \ReflectionClass($baseDO);
        $tableName = ($this->getTableObj($class))["tableName"];

        if (empty($tableName))
            return null;

        $properties = $class->getProperties();
        $conditions = [];

        foreach ($properties as $property)
        {
            $attribute = $this->getAttributeObj($property);

            // Only those column has value could be setup.
            if (isset($baseDO->{$property->name})) {
                $conditions[] = $attribute["column"]." = :".$attribute["column"];
            }
        }

        $conditionsCat = implode(' AND ', $conditions);

        $sql = "Delete FROM {$tableName} WHERE {$conditionsCat}";
        try {
            $conn = $this->getConnection();

            $stmt = $conn->prepare($sql);
            foreach ($properties as $property) {
                $attribute = $this->getAttributeObj($property);
                if (!is_null($baseDO->{$property->name}) && !is_null($attribute)) {
                    $stmt->bindValue(":" . $attribute["column"], $baseDO->{$property->name}, $this->PDOTypeArray[$attribute["type"]]);
                }
            }

            $stmt->execute();
            $result = $stmt->rowCount() >= 1;
            $this->errorMessage = $stmt->errorInfo()[2];
        } catch (\PDOException $exception) {
            $this->errorMessage = $exception->getMessage();
            $result = false;
        }

        return $result;
    }

    public function fetchByPrimaryKey(BaseDO &$baseDO) {
        $class     = new \ReflectionClass($baseDO);
        $tableName = ($this->getTableObj($class))["tableName"];

        if (empty($tableName))
            return null;

        $properties = $class->getProperties();
        $conditions = [];

        foreach ($properties as $property)
        {
            $attribute = $this->getAttributeObj($property);

            // Only those column has value could be setup.
            if (isset($baseDO->{$property->name}) && $attribute['PK']) {
                $conditions[] = $attribute["column"]." = :".$attribute["column"];
            }
        }

        $conditionsCat = implode(' AND ', $conditions);

        $sql = "SELECT * FROM {$tableName} WHERE {$conditionsCat}";

        try {
            $conn = $this->getConnection();

            $stmt = $conn->prepare($sql);
            foreach ($properties as $property) {
                $attribute = $this->getAttributeObj($property);
                if (!is_null($baseDO->{$property->name}) && !is_null($attribute)) {
                    $stmt->bindValue(":" . $attribute["column"], $baseDO->{$property->name}, $this->PDOTypeArray[$attribute["type"]]);
                }
            }

            $stmt->execute();
            $result = $row = $stmt->fetch(\PDO::FETCH_OBJ);
            $this->errorMessage = $stmt->errorInfo()[2];

            if ($result)
            {
                $baseDO->setFromDataRow($row);
            }
        } catch (\PDOException $exception) {
            $this->errorMessage = $exception->getMessage();
            $result = false;
        }

        return $result;
    }

    /**
     * @param BaseDO $baseDO
     * @param string $orderBy
     * @return array|null
     */
    public function fetchAll(BaseDO $baseDO, $orderBy = null) {
        $class     = new \ReflectionClass($baseDO);
        $tableName = ($this->getTableObj($class))["tableName"];

        if (empty($tableName))
            return null;

        $properties = $class->getProperties();

        $sql = "SELECT * FROM {$tableName} ";
        if ($orderBy)
        {
            $sql .= "ORDER BY {$orderBy}";
        }

        try {
            $conn = $this->getConnection();

            $stmt = $conn->prepare($sql);
            foreach ($properties as $property) {
                $attribute = $this->getAttributeObj($property);
                if (!is_null($baseDO->{$property->name}) && !is_null($attribute)) {
                    $stmt->bindValue(":" . $attribute["column"], $baseDO->{$property->name}, $this->PDOTypeArray[$attribute["type"]]);
                }
            }

            $stmt->execute();
            $result = [];
            $this->errorMessage = $stmt->errorInfo()[2];

            while ($row = $stmt->fetch(\PDO::FETCH_OBJ))
            {
                $baseDO = new BaseDO();
                $baseDO = $baseDO->castAs($class->getName());
                $baseDO->setFromDataRow($row);
                array_push($result, $baseDO);
            }
        } catch (\PDOException $exception) {
            $this->errorMessage = $exception->getMessage();
            $result = null;
        }

        return $result;
    }
}
