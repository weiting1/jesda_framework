<?php
/**
 * @author mei
 * @date 2017/10/12
 * @since 2017/10/12 寫入檔案的log
 */
namespace JesdaLib\Common;

use Monolog\Handler\AbstractProcessingHandler;

class FileLogHandler extends AbstractProcessingHandler
{
    public $path;

    /**
     * @param array $record
     */
    protected function write(array $record)
    {
        if(!$this->path){
            $this->path = PROJECT_PATH.'/log';
        }

        if(!file_exists($this->path)){
            mkdir($this->path, 0777, true);
        }

        $count = 1;
        do{
            $filePath = $this->path.'/'.'error_'.$count.'.log';
            if(!file_exists($filePath) || filesize($filePath) < 1024*1024*10){
                break;
            }
            $count++;
        } while(true);

        $file = fopen($filePath, 'a');

        $data = array(
            'channel' => $record['channel'],
            'level' => $record['level_name'],
            'message' => $record['message'],
            'context' => json_encode($record['context'], 320),
            'extra' => json_encode($record['extra'], 320),
            'datetime' => $record['datetime']->format('Y-m-d H:i:s'),
        );

        fwrite($file, json_encode($data, 320 + JSON_PRETTY_PRINT));
        fclose($file);
    }
}