<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 2017/7/21
 * Time: 下午4:11
 */

//自訂錯誤抓取
/**
 * @param $errorNo
 * @param $errorMessage
 * @param $errorFile
 * @param $errorLine
 * @return bool
 */
function errorHandler($errorNo, $errorMessage, $errorFile, $errorLine)
{
    if ($errorNo == E_ERROR || $errorNo == E_USER_ERROR) {
        $logHelper = new \JesdaLib\Common\LogHelper("GLOBAL");
        $logHelper->error("未處理的錯誤", ['ERROR', $errorMessage, $errorFile, $errorLine]);
    }
    if ($errorNo == E_WARNING || $errorNo == E_USER_WARNING)
    {
        $logHelper = new \JesdaLib\Common\LogHelper("GLOBAL");
        $logHelper->error("未處理的警告", ['WARNING', $errorMessage, $errorFile, $errorLine]);
    }

    return false;
}

/**
 * @return bool
 */
function fatalErrorHandler()
{
    $error = error_get_last();
    switch ($error['type']) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_WARNING:
        case E_USER_WARNING:

        // case E_PARSE:
        //     $slack = new \JesdaLib\Common\JesdaSlackHandler("FATAL");
        //     $slack->write(['channel' => 'FATAL', 'level' => 'FATAL', 'message' => $error['message']]);
        //     break;
        default:
            break;
    }
    return true;
}

register_shutdown_function("fatalErrorHandler");
set_error_handler("errorHandler");
