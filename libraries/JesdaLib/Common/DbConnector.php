<?php
/**
 * @author mei
 * @date 2017/7/25
 * @since 2017/7/25
 * 讀取ini取得資料庫連線資訊並產生pdo物件
 */

namespace JesdaLib\Common;

use JesdaLib\Common\DbAdapter\Mysql;
use JesdaLib\Common\DbAdapter\Sqlsrv;

class DbConnector
{
    public static $defaultIniPath = 'db';

    public static function getDb($iniPath = null)
    {
        try {
            #$log = new LogHelper("DB Connection");
            $reader = new IniReader();

            if (null === $iniPath) {
                $iniPath = self::$defaultIniPath;
            }

            $config = $reader->getConfig($iniPath);

            foreach ($config['dsn'] as $item =>$value){
                $config['dsn'][$item] = $item.'='.$value;
            }

            $dsn = $config['adapter'] . ':' . implode(';', $config['dsn']);
            $options = $config['options'] ?? [];

            switch ($config['adapter']){
                case 'mysql':
                    $pdo = new Mysql($dsn, $config['username'], $config['password'], $options);
                    $pdo->setAttribute(\pdo::ATTR_CASE, \pdo::CASE_LOWER);
                    $pdo->setAttribute(\pdo::ATTR_DEFAULT_FETCH_MODE, \pdo::FETCH_ASSOC);
                    break;
                case 'sqlsrv':
                    $pdo = new Sqlsrv($dsn, $config['username'], $config['password'], $options);
                    $pdo->setAttribute(\pdo::ATTR_CASE, \pdo::CASE_LOWER);
                    $pdo->setAttribute(\pdo::ATTR_DEFAULT_FETCH_MODE, \pdo::FETCH_ASSOC);
                    break;
            }

            if(!isset($pdo)){
                throw new \Exception('db adapter does not exist');
            }

            if (!empty($pdo->errorCode())) {
                #$log->error("errorInfo", $pdo->errorInfo());
                #$log->error("errorCode", $pdo->errorCode());
            }

            return $pdo;

        } catch (\PDOException $e){
            #$log->error("Exception when create connection: " . $e->getMessage(), $e);
            return null;
        } catch (\Exception $e){
            #$log->error("Exception before create connection: " . $e->getMessage(), $e);
            return null;
        }
    }
}