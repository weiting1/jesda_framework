<?php

namespace JesdaLib\Common;

use Monolog\Handler\AbstractProcessingHandler;

class PDOHandler extends AbstractProcessingHandler
{
    private $initialized = false;

    /**
     * @var \pdo
     */
    private $pdo;

    /**
     * @var \PDOStatement
     */
    private $statement;

    /**
     * PDOHandler constructor.
     * @param int $level
     * @param bool $bubble
     */
    public function __construct($level, $bubble = true)
    {
        $options = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4');
        if (DB_Server === "dblib")
            $this->pdo = new \PDO(DB_Server.":charset=UTF-8;host=".DB_Server_Name.":" . DB_Port.";dbname=".DB_DBName, DB_User_Name, DB_Password);
        else if (DB_Server === "sqlsrv")
            $this->pdo = new \PDO(DB_Server.":Server=".DB_Server_Name.",". DB_Port.";Database=".DB_DBName, DB_User_Name , DB_Password);
        else
            $this->pdo = new \PDO(DB_Server.":host=".DB_Server_Name.";port=".DB_Port.";dbname=".DB_DBName, DB_User_Name, DB_Password, $options);
        parent::__construct($level, $bubble);
    }

    /**
     * @param array $record
     */
    protected function write(array $record)
    {
        if (!$this->initialized) {
            $this->initialize();
        }

//        if (is_array($record['context'])) {
//            if (!empty($record['context']['pwd'])) {
//                $pwd = $record['context']['pwd'];
//                $record['context']['pwd'] = (openssl_encrypt($pwd, "AES-256-OFB", AES_Key, 0, AES_IV));
//            }
//            if (!empty($record['context']['password'])) {
//                $pwd = $record['context']['password'];
//                $record['context']['password'] = (openssl_encrypt($pwd, "AES-256-OFB", AES_Key, 0, AES_IV));
//            }
//            if (!empty($record['context']['oldPwd'])) {
//                $pwd = $record['context']['oldPwd'];
//                $record['context']['oldPwd'] = (openssl_encrypt($pwd, "AES-256-OFB", AES_Key, 0, AES_IV));
//            }
//            if (!empty($record['context']['newPwd'])) {
//                $pwd = $record['context']['newPwd'];
//                $record['context']['newPwd'] = (openssl_encrypt($pwd, "AES-256-OFB", AES_Key, 0, AES_IV));
//            }
//        }

        $this->statement->execute(array(
            'channel' => $record['channel'],
            'level' => $record['level_name'],
            'message' => $record['message'],
            'context' => json_encode($record['context'], 320), //str_replace($record['channel'].'.'.$record['level_name'].': ', '', $record['formatted']),
            'extra' => json_encode($record['extra'], 320),
            'datetime' => $record['datetime']->format('Y-m-d H:i:s'),
        ));
    }

    /**
     *
     */
    private function initialize()
    {
        $this->statement = $this->pdo->prepare(
            'INSERT INTO monolog (channel, level, message, context, extra, datetime) VALUES (:channel, :level, :message, :context, :extra, :datetime)'
        );

        $this->initialized = true;
    }
}