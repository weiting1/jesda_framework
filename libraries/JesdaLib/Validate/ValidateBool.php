<?php
/**
 * @author mei
 * @date 2017/6/16
 * @since 2017/6/16 檢查數值須小於max
 *
 * $lessThan->isValid($val, 100);
 * $lessThan->isValid($val, ['max'=>100]);
 */
namespace JesdaLib\Validate;

use Respect\Validation\Validator as v;

class ValidateBool extends BaseValidate
{
    const INVALID = 'invalid';

    protected $messageVariables = [];

    protected $messageTemplates = [
        self::INVALID => '{{value}} is invalid, 只能是true / false',
    ];

    protected function validate($value)
    {
        if(!v::boolVal()->validate($value)){
            $this->error(self::INVALID);
            return false;
        }

        return true;
    }
}