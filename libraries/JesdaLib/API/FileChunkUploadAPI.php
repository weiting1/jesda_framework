<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 3/23/17
 * Time: 15:17
 */

namespace JesdaLib\API;

use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\LibStatusDO;

class FileChunkUploadAPI extends BaseAPI
{
    private $logHelper;
    //實際上儲存檔案的路徑請依據後端的需求自行改寫
    private $folder = "/upload/";
    private $keepName = false;

    public function __construct(BaseAPI $functionAPI = null)
    {
        $this->appendAuth("GET", '*');
        $this->appendAuth("POST", '*');
        $this->appendAuth("PUT", '*');
        $this->appendAuth("DELETE", '*');

        $this->logHelper = new LogHelper(get_class($this));
        parent::__construct($functionAPI);
    }

    protected function setConfig($folder, $keepName)
    {
        $this->keepName = $keepName;
        $this->folder = $folder;
    }

    protected function setFolder($folder)
    {
        $this->folder = $folder;
    }

    protected function get()
    {
        $this->functionAPI->inputs();
    }

    protected function post()
    {
        if (empty($_FILES) || $_FILES['file']['error']) {
            $this->sendResponse(400, LibStatusDO::Upload_File_Fail);
        }

        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
        $filePath = $fileName;

        $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
        if ($out) {
            // Read binary input stream and append it to temp file
            $in = @fopen($_FILES['file']['tmp_name'], "rb");

            if ($in) {
                while ($buff = fread($in, 4096))
                    fwrite($out, $buff);
            } else
                $this->sendResponse(400, LibStatusDO::Upload_File_Fail);

            @fclose($in);
            @fclose($out);

            @unlink($_FILES['file']['tmp_name']);
        } else
            $this->sendResponse(400, LibStatusDO::Upload_File_Fail);

        if (!$chunks || $chunk == $chunks - 1) {
            $extName = explode('.',$_FILES["file"]["name"]);

            if ($extName[sizeof($extName)-1] == "php") {
                $this->sendResponse(406);
                die();
            }
            else {
                $target_file = $this->folder
                    . ($this->keepName ? $_FILES["file"]["name"] :
                        (hash_hmac('sha256', $_FILES["file"]["name"] . time(), '12345', false)
                            . '.' . $extName[sizeof($extName) - 1]));

                $http = $_SERVER["HTTPS"] ? ($_SERVER["HTTPS"] != "on" ? "http" : "https") : "http";
                $metadata['uploadedFileURL'] = $http . '://' . $_SERVER['HTTP_HOST'] . $target_file;

                if (!file_exists(__DIR__ . '/../../../../..' . $this->folder))
                    mkdir(__DIR__ . '/../../../../..' . $this->folder);

                rename("{$filePath}.part", __DIR__ . '/../../../../..' . $target_file);
                if (is_null($this->functionAPI))
                    $this->sendResponse(200, $this->myJSONEncode($metadata));
                else
                    $this->functionAPI->inputs($metadata);
            }
        }
    }

    protected function put()
    {
        //沒有實作修改檔案, 其實應該也沒有必要
        $this->functionAPI->inputs();
    }

    protected function delete()
    {
        //沒有實作刪除server 上檔案, 有需要再說
        $this->functionAPI->inputs();
    }
}