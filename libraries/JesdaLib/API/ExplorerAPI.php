<?php
namespace JesdaLib\API;

use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\LibStatusDO;

class ExplorerAPI extends BaseAPI
{
    private $logHelper;

    public function __construct(BaseAPI $functionAPI = null)
    {
        $this->appendAuth("GET", '*');
        $this->appendAuth("POST", '*');
        $this->appendAuth("PUT", '*');
        $this->appendAuth("DELETE", '*');

        $this->logHelper = new LogHelper(get_class($this));
        parent::__construct($functionAPI);
    }

    protected function get()
    {

        $root = __DIR__."/../../../../../upload/explorer/";
        $data = $this->dir_to_array($root, 'Root');
        $data = json_encode($data);

        echo $data;
    }

    protected function post()
    {

    }

    protected function put()
    {

    }

    protected function delete()
    {

    }

    private function dir_to_array($dir, $givenName)
    {
        $data = [];
        $data['name'] = $givenName;
        $data['children'] = [];

        foreach (new \DirectoryIterator($dir) as $f) {
            if ($f->isDot()) {
                continue;
            }

            $path = $f->getPathname();
            $name = $f->getFilename();

            if ($f->isFile()) {
                array_push($data['children'], [ 'name' => $name, 'size' => $f->getSize(), 'date' => date("Y-m-d H:i:s", $f->getMTime())]);
            } else {
                $files = $this->dir_to_array($path, $name);
                array_push($data['children'], $files);
            }
        }

        if (empty($data['children']))
            array_push($data['children'], []);
        return $data;
    }
}