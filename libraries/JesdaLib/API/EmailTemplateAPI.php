<?php
namespace JesdaLib\API;

use JesdaLib\BusinessRule\EmailTemplateBR;
use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\LibStatusDO;

class EmailTemplateAPI extends BaseAPI
{
    private $logHelper;

    public function __construct(BaseAPI $functionAPI = null)
    {
        $this->appendAuth("GET", '*');
        $this->appendAuth("POST", '*');
        $this->appendAuth("PUT", '*');
        $this->appendAuth("DELETE", '*');

        $this->logHelper = new LogHelper(get_class($this));
        parent::__construct($functionAPI);
    }

    protected function get()
    {
        $br = new EmailTemplateBR();
        $result = $br->getAllTemplate($this->metadata['id']);
        if (is_null($result))
            $this->sendResponse(400, LibStatusDO::EmailTemplate_Fail);
        else
            $this->sendResponse(200, $this->myJSONEncode($result));
    }

    protected function post()
    {
        $this->logHelper->info("POST", $this->_request);
        $br = new EmailTemplateBR();

        //只接受array 形式, F注意JSON內只接受雙引號
        if (empty($this->_request["title"]) || empty($this->_request["content"]) || empty($this->_request["templateName"])) {
            $this->sendResponse(400, LibStatusDO::EmailTemplate_Fail);
        }
        else {
            $result = $br->saveTemplate($this->_request['id'], $this->_request['title'], $this->_request["content"], $this->_request["templateName"], $this->metadata['id']);
            if ($result)
                $this->sendResponse(200, LibStatusDO::OK);
            else
                $this->sendResponse(400, LibStatusDO::EmailTemplate_Fail);
        }
    }

    protected function put()
    {
        $this->sendResponse(406);
    }

    protected function delete()
    {
        $br = new EmailTemplateBR();
        if (empty($this->_request["id"])) {
            $this->sendResponse(400, LibStatusDO::EmailTemplate_Fail);
        }
        else {
            $result = $br->deleteTemplate($this->_request['id']);
            if ($result)
                $this->sendResponse(200, LibStatusDO::OK);
            else
                $this->sendResponse(400, LibStatusDO::EmailTemplate_Fail);
        }
    }
}