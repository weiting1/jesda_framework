<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 2017/8/23
 * Time: 下午2:55
 */
namespace JesdaLib\API\SendMail;

use JesdaLib\API\BaseAPI;
use JesdaLib\Common\DBConfiguration;
use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\CommonMailboxDO;
use JesdaLib\DataObject\LibStatusDO;

class SendMailAPI extends BaseAPI
{

    private $logHelper;

    public function __construct(BaseAPI $functionAPI = null)
    {
        $this->appendAuth("GET", '*');
        $this->appendAuth("POST", '*');
        $this->appendAuth("PUT", '*');
        $this->appendAuth("DELETE", '*');

        $this->logHelper = new LogHelper(get_class($this));
        parent::__construct($functionAPI);
    }

    protected function get()
    {
        $a = is_array(Mail_FromList) ? Mail_FromList : ["sender" => Mail_FromName.':'.Mail_From];
        $this->sendResponse(200, $this->myJSONEncode($a));
    }

    protected function post()
    {
        //寄件
        $sender = $this->_request['sender'];
        $sender = empty($sender) ? Mail_FromName : $sender;
        $receiverList = is_array($this->_request['receiverList']) ? $this->_request['receiverList'] : json_decode($this->_request['receiverList'],false);
        $title = $this->_request['title'];
        $content = $this->_request['content'];

        $do = new CommonMailboxDO();
        $do->sender = $sender;
        $do->receiver = json_encode($receiverList);
        $do->title = $title;
        $do->content = $content;
        $do->updatedBy = $GLOBALS["USER"];
        $do->updatedAt = date("Y-m-d H:i:s");
        
        $db = new DBConfiguration();
        $db->new($do);

        $mail = new \PHPMailer();
        $mail->CharSet = 'UTF-8';
        mb_internal_encoding('UTF-8');

        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth = Mail_Auth;                  // enable SMTP authentication
        $mail->SMTPSecure = Mail_Protocal;
        $mail->SMTPAutoTLS = false;
        $mail->Host = Mail_Host;      // SMTP server
        $mail->Port = Mail_Port;                   // SMTP port
        $mail->Username = Mail_User;  // username
        $mail->Password = Mail_Password;            // password

        $mail->SetFrom(Mail_From, $sender);
        $mail->AddReplyTo(Mail_From, $sender);

        $mail->Subject = $title;
        $mail->MsgHTML($content);

        $result = true;
        foreach ($receiverList as $receiver) {
            $mail->AddAddress($receiver, $receiver);

            if (!$mail->Send()) {
                $result = false;
                break;
            }
        }

        if ($result)
        {
            $this->sendResponse(200, LibStatusDO::OK);
        }
        else
        {
            $this->logHelper->error("Mailer Error: " . $mail->ErrorInfo);
            $this->sendResponse(400, LibStatusDO::EmailNotification_Sent_Fail);
        }
    }

    protected function put()
    {
        $this->sendResponse(406);
    }

    protected function delete()
    {
        $this->sendResponse(406);
    }
}