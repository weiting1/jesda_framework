<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 2017/8/3
 * Time: 下午11:10
 */

namespace JesdaLib\API;

use JesdaLib\BusinessRule\EmailTemplateBR;
use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\LibStatusDO;

class EmailNotificationAPI extends BaseAPI
{
    private $logHelper;

    public function __construct(BaseAPI $functionAPI = null)
    {
        $this->appendAuth("GET", '*');
        $this->appendAuth("POST", '*');
        $this->appendAuth("PUT", '*');
        $this->appendAuth("DELETE", '*');

        $this->logHelper = new LogHelper(get_class($this));
        parent::__construct($functionAPI);
    }

    protected function get()
    {
        $this->send();
    }

    protected function post()
    {
        $this->send();
    }

    protected function put()
    {
        $this->send();
    }

    protected function delete()
    {
        $this->send();
    }

    private function send()
    {
        $templateId = $this->metadata['templateId'];
        $replaceContent = $this->metadata['replaceContent'];
        $toAddress = $this->metadata['toAddress'];
        $toCC = $this->metadata['toCC'];

        if (empty($templateId) || empty($replaceContent) || empty($toAddress))
        {
            $this->logHelper->warn("EMAIL NOTIFICATION DATA EMPTY", $this->metadata);
            $this->sendResponse(400, LibStatusDO::EmailNotification_Data_Empty);
        }
        else {
            $emailBR = new EmailTemplateBR();
            $emailDO = $emailBR->getTemplateByTemplateId($templateId);

            $mail = new \PHPMailer();
            $mail->CharSet = 'UTF-8';
            mb_internal_encoding('UTF-8');

            $mail->IsSMTP(); // telling the class to use SMTP
            $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
            $mail->SMTPAuth = Mail_Auth;                  // enable SMTP authentication
            $mail->SMTPSecure = Mail_Protocal;
            $mail->SMTPAutoTLS = false;
            $mail->Host = Mail_Host;      // SMTP server
            $mail->Port = Mail_Port;                   // SMTP port
            $mail->Username = Mail_User;  // username
            $mail->Password = Mail_Password;            // password

            $mail->SetFrom(Mail_From, Mail_FromName);
            $mail->AddReplyTo(Mail_From, Mail_FromName);

            $mail->Subject = $emailDO->title;
            foreach($replaceContent as $key => $value)
            {
                $emailDO->content = str_replace("{".$key."}", $value, $emailDO->content);
            }
            $mail->MsgHTML($emailDO->content);

            $mail->AddAddress($toAddress, $toAddress);

            if (!empty($toCC))
                $mail->AddCC($toCC, $toCC);

            if (!$mail->Send()) {
                $this->logHelper->error("Mailer Error: " . $mail->ErrorInfo);
                //$this->sendResponse(400, LibStatusDO::EmailNotification_Sent_Fail);
            }

        }
    }

}