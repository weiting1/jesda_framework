<?php
namespace JesdaLib\API;

use JesdaLib\BusinessRule\ExcelTemplateBR;
use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\LibStatusDO;

class ExcelTemplateAPI extends BaseAPI
{
    private $logHelper;

    public function __construct(BaseAPI $functionAPI = null)
    {
        $this->appendAuth("GET", '*');
        $this->appendAuth("POST", '*');
        $this->appendAuth("PUT", '*');
        $this->appendAuth("DELETE", '*');

        $this->logHelper = new LogHelper(get_class($this));
        parent::__construct($functionAPI);
    }

    protected function get()
    {
        $br = new ExcelTemplateBR();
        $result = $br->getAllTemplate();
        if (is_null($result))
            $this->sendResponse(400, LibStatusDO::ExcelTemplate_Fail);
        else
            $this->sendResponse(200, $result);
    }

    protected function post()
    {
        $this->logHelper->info("POST", $this->_request);
        $br = new ExcelTemplateBR();

        //只接受array 形式, F注意JSON內只接受雙引號
        if (!is_array(json_decode($this->_request["columns"], true))) {
            $this->sendResponse(400, LibStatusDO::ExcelTemplate_Columns_Should_be_Array_Fail);
        }
        else {
            $result = $br->saveTemplate($this->_request['api'], $this->_request['templateName'], $this->_request["columns"]);
            if ($result)
                $this->sendResponse(200, LibStatusDO::OK);
            else
                $this->sendResponse(400, LibStatusDO::ExcelTemplate_Fail);
        }
    }

    protected function put()
    {
        $this->sendResponse(406);
    }

    protected function delete()
    {
        $this->sendResponse(406);
    }
}