<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 3/23/17
 * Time: 15:17
 */

namespace JesdaLib\API;

use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\LibStatusDO;

class FileUploadAPI extends BaseAPI
{
    private $logHelper;
    //實際上儲存檔案的路徑請依據後端的需求自行改寫
    public $folder = "/upload/";
    public $keepName = false;

    public function __construct(BaseAPI $functionAPI = null)
    {
        $this->appendAuth("GET", '*');
        $this->appendAuth("POST", '*');
        $this->appendAuth("PUT", '*');
        $this->appendAuth("DELETE", '*');

        $this->logHelper = new LogHelper(get_class($this));
        parent::__construct($functionAPI);
    }

    protected function setConfig($folder, $keepName)
    {
        $this->keepName = $keepName;
        $this->folder = $folder;
    }

    protected function setFolder($folder)
    {
        $this->folder = $folder;
    }

    protected function get()
    {
        $this->functionAPI->inputs();
    }

    protected function post()
    {
        //支援多個檔案同時上傳
        //後端工程師請知會前端你希望的變數 ($key)
        //然後可以在resource API中, 利用$this->metadata[$key] 取得URL
        $this->logHelper->info("POST", $_FILES);

        $file = $_FILES['file'];
        $fileNameList = $file['name'];
        $metadata = [];
        if (!empty($fileNameList)) {
            foreach ($fileNameList as $key => $originalName) {

                $extName = explode('.', $originalName);

                if (strtolower($extName[sizeof($extName) - 1]) == "php") {
                    $this->sendResponse(406);
                    die();
                } else {
                    $target_file = $this->folder
                        . ($this->keepName ? $originalName :
                            (hash_hmac('sha256', $originalName . time(), '12345', false)
                                . '.' . $extName[sizeof($extName) - 1]));

                    //依據實際需求判斷要不要加上HTTP_HOST
                    $http = $_SERVER["HTTPS"] ? ($_SERVER["HTTPS"] != "on" ? "http" : "https") : "http";
                    $metadata[$key] = $http . '://' . $_SERVER['HTTP_HOST'] . $target_file;

                    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $this->folder))
                        mkdir($_SERVER['DOCUMENT_ROOT'] . $this->folder, 0777, true);

                    if (!move_uploaded_file($file["tmp_name"][$key], $_SERVER['DOCUMENT_ROOT'] . $target_file)) {
                        $metadata[$key] = '';
                        $this->logHelper->error("POST", [$file["tmp_name"][$key], $_SERVER['DOCUMENT_ROOT'] . $target_file]);
                    }
                }
            }
//            $_SERVER['DOCUMENT_ROOT']
        }

        if (is_null($this->functionAPI))
            $this->sendResponse(200, LibStatusDO::OK);
        else
            $this->functionAPI->inputs($metadata);
    }

    protected function put()
    {
        //沒有實作修改檔案, 其實應該也沒有必要
        $this->functionAPI->inputs();
    }

    protected function delete()
    {
        //沒有實作刪除server 上檔案, 有需要再說
        $this->functionAPI->inputs();
    }
}