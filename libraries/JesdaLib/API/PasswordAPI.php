<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 3/23/17
 * Time: 15:17
 */

namespace JesdaLib\API;

use JesdaLib\BusinessRule\PasswordBR;
use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\LibStatusDO;
use JesdaLib\DataObject\MetaData\EmailNotificationDO;

class PasswordAPI extends BaseAPI
{
    private $logHelper;
    private $tableName;
    private $emailColumnName;
    private $passwordColumnName;
    private $templateId = "reset_password";

    public function __construct(BaseAPI $functionAPI = null)
    {
        $this->appendAuth("POST", '*');
        $this->appendAuth("DELETE", '*');

        $this->logHelper = new LogHelper(get_class($this));
        parent::__construct($functionAPI);
    }

    protected function get()
    {
        $this->sendResponse(406);
    }

    protected function post()
    {
        $this->logHelper->info("POST", $this->_request);
        //重設密碼
        $jwt = $this->_request["jwt"];
        $password = $this->_request["password"];
        if (!empty($jwt) && !empty($password))
        {
            $passwordBR = new PasswordBR();
            $result = $passwordBR->resetPasswordByJWT($jwt, $password);

            if ($result)
                $this->sendResponse(200, LibStatusDO::OK);
            else
                $this->sendResponse(400, LibStatusDO::Reset_Password_Fail);
        }
        else
        {
            $this->sendResponse(400, LibStatusDO::Reset_Password_Fail);
        }
    }

    protected function put()
    {
        $this->sendResponse(406);
    }

    protected function delete()
    {
        $this->logHelper->info("DELETE", $this->_request);
        //要求重設密碼
        $email = $this->_request["email"];
        if (!empty($email) && !empty($this->tableName) && !empty($this->functionAPI))
        {
            $passwordBR = new PasswordBR();
            $jwt = $passwordBR->getJWTtoResetPassword($email, $this->tableName, $this->emailColumnName, $this->passwordColumnName);

            if (empty($jwt))
            {
                $this->sendResponse(400, LibStatusDO::Reset_Password_Fail_Cant_Find_Email);
            }
            else {
                $notificationDO = new EmailNotificationDO();
                $notificationDO->setTemplateId($this->templateId);
                $notificationDO->setReplaceContent(["JWT" => $jwt]);
                $notificationDO->setToAddress($email);

                $this->functionAPI->inputs($notificationDO->metadata);
            }
        }
        else
        {
            $this->sendResponse(400, LibStatusDO::Change_Password_Request_Data_Empty);
        }
    }

    public function setTableName($tableName, $emailColumnName, $passwordColumnName, $templateId)
    {
        $this->tableName = $tableName;
        $this->emailColumnName = $emailColumnName;
        $this->passwordColumnName = $passwordColumnName;
        if (!empty($templateId))
            $this->templateId = $templateId;
    }
}