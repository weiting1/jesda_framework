<?php

namespace JesdaLib\API;

use JesdaLib\Common\DBConfiguration;
use JesdaLib\Common\LogHelper;
use JesdaLib\DataObject\LibStatusDO;

abstract class BaseAPI
{

    protected $_request = array();
    /**
     * 設定role 與API存取權限之關聯
     * 完全不設定的時候預設是無法存取
     * 基於安全考量請審慎設定
     * * 表示匿名存取
     */
    private $APIRolePermission = [
        "GET" => null,
        "POST" => null,
        "PUT" => null,
        "DELETE" => null
    ];

    protected $statusDO;
    protected $baseDO;
    protected $metadata;

    abstract protected function get();

    abstract protected function post();

    abstract protected function put();

    abstract protected function delete();

    /**
     * @param $method
     * @param $role
     */
    protected function appendAuth($method, $role)
    {
        if (!empty($role) && is_null($this->APIRolePermission[$method]))
            $this->APIRolePermission[$method] = [];

        array_push($this->APIRolePermission[$method], $role);
    }

    /**
     * @var BaseAPI
     */
    protected $functionAPI;

    protected $tokenId;

    public function __construct(BaseAPI $functionAPI = null)
    {
        $this->statusDO = new LibStatusDO();
        $this->functionAPI = $functionAPI;
        $this->tokenId = $_SERVER['HTTP_X_JESDA_API_KEY'] ?? null;
    }

    protected function sendResponse($status = 200, $responseBody = '', $content_type = "application/json")
    {
        if (!headers_sent()) {
            $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->getHttpStatusMessage($status);

            header($status_header);
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X-Jesda-Api-Key");
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            
            if (empty(json_decode($responseBody)))
                $content_type = "text/plain";

            header('Content-type: ' . $content_type . ";charset=utf-8");
        }

        $json = (strlen($responseBody) == 3) ? $this->statusDO->getStatus($responseBody) : $responseBody;
        echo $json;
    }

    private function getHttpStatusMessage($status)
    {
        $codes = Array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            203 => 'Non-Authoritative Information',
            400 => 'Bad Request',
            404 => 'Page Not Founds',
            406 => "Not Acceptable",
            500 => 'Internal Server Error',
            505 => 'HTTP Version Not Supported'
        );

        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    protected function getRequestMethod()
    {
        return filter_input(INPUT_SERVER, 'REQUEST_METHOD');
    }

    /**
     * @param $metadata
     */
    public function inputs($metadata = null)
    {
        $this->metadata = $metadata;
        $authRole = is_array($GLOBALS['ROLE']) ? $GLOBALS['ROLE'] : json_decode($GLOBALS['ROLE'], true);

        $acceptedRole = $this->APIRolePermission[$this->getRequestMethod()];
        $pass = is_null($acceptedRole) ? false : $acceptedRole[0] === '*';
        $pass = $pass || $this->getRequestMethod() === "OPTIONS";

        if (!$pass) {
            if (is_array($acceptedRole) && is_array($authRole)) {
                foreach ($authRole as $role) {
                    if (in_array($role, $acceptedRole)) {
                        $pass = true;
                        break;
                    }
                }
            }
        }

        if (!$pass) {
            $this->sendResponse(406, LibStatusDO::API_Access_Denied);
            die();
        } else {

            switch ($this->getRequestMethod()) {
                case "POST":
                    $this->_request = filter_input_array(INPUT_POST);
                    $action = $this->_request['action'];
                    if (!is_null($this->baseDO)) {
                        $this->postSwitch($action);
                    } else {
                        $this->post();
                    }
                    break;
                case "GET":
                    $this->_request = filter_input_array(INPUT_GET);
                    $this->get();
                    break;
                case "DELETE":
                    parse_str(file_get_contents('php://input'), $this->_request);
                    $action = $this->_request['action'];
                    if (!is_null($this->baseDO)) {
                        $this->deleteSwitch($action);
                    } else {
                        $this->delete();
                    }
                    break;
                case
                "PUT":
                    parse_str(file_get_contents("php://input"), $this->_request);
                    $this->put();
                    break;
                case "OPTIONS":
                    $this->sendResponse();
                    break;
            }
        }
    }

    protected function myJSONEncode($objectFromBR)
    {
        $objectToFrontend = json_encode($objectFromBR, 320);
        $objectToFrontend = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/', '', $objectToFrontend);
        $objectToFrontend = preg_replace('/\"password\"\:\".*?\"\,/s', '', $objectToFrontend);
        $objectToFrontend = preg_replace('/\"pwd\"\:\".*?\"\,/s', '', $objectToFrontend);

        return $objectToFrontend;
    }

    private function postSwitch($action)
    {
        switch ($action) {
            case "updateColumns":
                $logHelper = new LogHelper("BaseAPI");
                $db = new DBConfiguration();
                $columns = $this->_request['columns'];
                $id = $this->_request['id'];

                if (!empty($this->tokenId)) {

                    $this->baseDO->id = $id;
                    $this->baseDO->columns = $columns;
                    $result = $db->modify($this->baseDO);
                    if (!$result) {
                        $logHelper->error("wizard update column fail" . $db->errorMessage, $this->baseDO);
                        $this->sendResponse(400, LibStatusDO::Update_Columns_Fail);
                    }
                    else
                    {
                        $this->sendResponse(200, LibStatusDO::OK);
                    }
                }
                else
                {
                    $this->sendResponse(400, LibStatusDO::Session_Invalid);
                }

                $db->closeConnection();
                break;
            case "switchSEQ":
                if (empty($this->tokenId)) {
                    $this->sendResponse(400, LibStatusDO::Session_Invalid);
                }
                else
                {
                    $logHelper = new LogHelper("BaseAPI");
                    $logHelper->info("switchSeq", $this->_request);

                    $class     = new \ReflectionClass($this->baseDO);
                    $comment = $class->getDocComment();
                    preg_match('/\{(.|\n)*\}/', $comment, $match);
                    $tableName = json_decode($match[0], true)["tableName"];

                    $db = new DBConfiguration();

                    $originalSEQ = $this->_request['originalSEQ'];
                    $newSEQ = $this->_request['newSEQ'];
                    $id = $this->_request['id'];

                    if (empty($id) || empty($newSEQ) || empty($originalSEQ))
                    {
                        $this->sendResponse(400, LibStatusDO::Update_SEQ_Empty);
                    }
                    else {
                        $conn = $db->getConnection();
                        $conn->beginTransaction();

                        if ($newSEQ < $originalSEQ) {
                            $sql = "update {$tableName} set seq = seq + 1 where seq BETWEEN {$newSEQ} AND :to";
                            $stmt = $db->getSQLStatement($sql);
                            $stmt->bindValue(":to", ($originalSEQ - 1));
                            $stmt->execute();
                        }

                        if ($newSEQ > $originalSEQ) {
                            $sql = "update {$tableName} set seq = seq - 1 where seq BETWEEN :to AND {$newSEQ}";
                            $stmt = $db->getSQLStatement($sql);
                            $stmt->bindValue(":to", ($originalSEQ + 1));
                            $stmt->execute();
                        }
                        $sql = "update {$tableName} set seq = {$newSEQ} where id = :id";

                        $stmt = $db->getSQLStatement($sql);
                        $stmt->bindValue(":id", $id);
                        $stmt->execute();
                        $conn->commit();

                        if ($stmt->errorCode() == "0000")
                        {
                            $this->sendResponse(200, LibStatusDO::OK);
                        }
                        else
                        {
                            $this->sendResponse(400, LibStatusDO::Update_SEQ_Fail);
                        }
                        $db->closeConnection();
                    }
                }
                break;
            default:
                $this->post();
        }
    }

    private function deleteSwitch($action)
    {
        switch ($action) {
            case "trash":
            case "recovery":
                $logHelper = new LogHelper("BaseAPI");
                $db = new DBConfiguration();
                $idList = json_decode($this->_request['idList'], true);
                foreach ($idList as $id) {
                    $this->baseDO->id = $id;
                    $this->baseDO->trash = ($action == "trash") ? 1 : 0;
                    $result = $db->modify($this->baseDO);
                    if (!$result) {
                        $logHelper->error("wizard trash DO fail" . $db->errorMessage, $this->baseDO);
                        $this->sendResponse(400);
                        die();
                    }
                }
                $this->sendResponse(200, LibStatusDO::OK);
                $db->closeConnection();
                break;
            case "realDelete":
                $logHelper = new LogHelper("BaseAPI");
                $db = new DBConfiguration();
                $idList = json_decode($this->_request['idList'], true);
                foreach ($idList as $id) {
                    $this->baseDO->id = $id;
                    $result = $db->destroy($this->baseDO);
                    if (!$result) {
                        $logHelper->error("wizard delete DO fail" . $db->errorMessage, $this->baseDO);
                        $this->sendResponse(400);
                        die();
                    }
                }
                $this->sendResponse(200, LibStatusDO::OK);
                $db->closeConnection();
                break;
            default:
                $this->delete();

        }
    }
}

?>
